<?php

namespace App\Test;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;

class CustomApiTestCase extends ApiTestCase
{
    protected $client;

    protected function init()
    {
        $this->client = static::createClient();
    }

    protected function get($uri, $data = [])
    {
        return $this->client->request("GET", $uri);
    }

    protected function post($uri, $data)
    {
        return $this->client->request(
            "POST",
            $uri,
            [
                "headers" => [
                    "Content-Type" => "application/ld+json",
                ],
                "json" => $data,
            ]
        );
    }

    protected function put($uri, $data = [])
    {
        return $this->client->request(
            "PUT",
            $uri,
            [
                "headers" => [
                    "Content-Type" => "application/ld+json",
                ],
                "json" => $data,
            ]
        );
    }

    protected function delete($uri)
    {
        return $this->client->request("DELETE", $uri);
    }

    protected function login($user, $password = null)
    {
        $this->post("/api/login", [
            "username" => $user->getUsername(),
            "password" => $password ?? "123",
        ]);
        $this->assertResponseIsSuccessful();
    }
}
