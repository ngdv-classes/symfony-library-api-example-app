<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use App\Repository\BookRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: BookRepository::class)]
#[ApiResource(
    operations: [
        new Post(
            denormalizationContext: ["groups" => ["books:write"]]
        ),
        new Get(
            normalizationContext: ["groups" => ["books:read", "books:item:read"]]
        ),
        new GetCollection(
            normalizationContext: ["groups" => ["books:read", "books:collection:read"]]
        ),
    ]
)]
class Book
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["books:read"])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(["books:read", "books:write"])]
    private ?string $name = null;

    #[ORM\ManyToOne(inversedBy: 'books')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(["books:read", "books:write"])]
    private ?Author $author = null;

    /**
     * @var Collection<int, Tag>
     */
    #[ORM\ManyToMany(targetEntity: Tag::class)]
    #[Groups(["books:item:read", "books:write"])]
    private Collection $tags;

    /**
     * @var Collection<int, BookGenre>
     */
    #[ORM\OneToMany(targetEntity: BookGenre::class, mappedBy: 'book', orphanRemoval: true)]
    #[Groups(["books:item:read"])]
    private Collection $bookGenres;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->bookGenres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getAuthor(): ?Author
    {
        return $this->author;
    }

    public function setAuthor(?Author $author): static
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection<int, Tag>
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): static
    {
        if (!$this->tags->contains($tag)) {
            $this->tags->add($tag);
        }

        return $this;
    }

    public function removeTag(Tag $tag): static
    {
        $this->tags->removeElement($tag);

        return $this;
    }

    /**
     * @return Collection<int, BookGenre>
     */
    public function getBookGenres(): Collection
    {
        return $this->bookGenres;
    }

    public function addBookGenre(BookGenre $bookGenre): static
    {
        if (!$this->bookGenres->contains($bookGenre)) {
            $this->bookGenres->add($bookGenre);
            $bookGenre->setBook($this);
        }

        return $this;
    }

    public function removeBookGenre(BookGenre $bookGenre): static
    {
        if ($this->bookGenres->removeElement($bookGenre)) {
            // set the owning side to null (unless already changed)
            if ($bookGenre->getBook() === $this) {
                $bookGenre->setBook(null);
            }
        }

        return $this;
    }
}
