<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class SecurityController extends AbstractController
{
    #[Route('/api/login', name: 'app_api_login')]
    public function login(#[CurrentUser] ?User $user): JsonResponse
    {
        if (!$user) {
            return $this->json([
                "message" => "Invalid credentials"
            ], 401);
        }

        return $this->json([
            "username" => $user->getUsername(),
        ]);
    }

    #[Route(path: '/api/logout', name: 'app_api_logout')]
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route(path: '/api/admin_resource', name: 'app_api_admin_resource')]
    public function adminResource(): JsonResponse
    {
        return $this->json([]);
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/api/user_resource', name: 'app_api_user_resource')]
    public function userResource(): JsonResponse
    {
        return $this->json([]);
    }

    #[Route(path: '/api/public_resource', name: 'app_api_public_resource')]
    public function publicResource(): JsonResponse
    {
        return $this->json([]);
    }
}
