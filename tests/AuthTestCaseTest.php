<?php

namespace App\Tests;

use App\Factory\UserFactory;
use App\Test\CustomApiTestCase;

class AuthTestCaseTest extends CustomApiTestCase
{
    public function testUnauthorizedUser(): void
    {
        $this->init();

        $this->get('/api/admin_resource');
        $this->assertResponseStatusCodeSame(401);

        $this->get('/api/user_resource');
        $this->assertResponseStatusCodeSame(401);

        $this->get('/api/public_resource');
        $this->assertResponseStatusCodeSame(200);
    }

    public function testUser(): void
    {
        $this->init();

        $user = UserFactory::createOne();
        $this->login($user);

        $this->get('/api/admin_resource');
        $this->assertResponseStatusCodeSame(403);

        $this->get('/api/user_resource');
        $this->assertResponseStatusCodeSame(200);

        $this->get('/api/public_resource');
        $this->assertResponseStatusCodeSame(200);
    }

    public function testAdmin(): void
    {
        $this->init();

        $admin = UserFactory::createOne(["roles" => ["ROLE_ADMIN"]]);
        $this->login($admin);

        $this->get('/api/admin_resource');
        $this->assertResponseStatusCodeSame(200);

        $this->get('/api/user_resource');
        $this->assertResponseStatusCodeSame(200);

        $this->get('/api/public_resource');
        $this->assertResponseStatusCodeSame(200);
    }
}
