<?php

namespace App\Tests;

use App\Factory\TagFactory;
use App\Test\CustomApiTestCase;

class TagResourceTest extends CustomApiTestCase
{
    public function testGet(): void
    {
        $this->init();

        $tag = TagFactory::createOne(["name" => "Tag1"]);
        TagFactory::createOne(["name" => "Tag2"]);

        $this->get('/api/tags');
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            "hydra:totalItems" => 2,
            "hydra:member" => [
                ["name" => "Tag1"],
                ["name" => "Tag2"],
            ],
        ]);

        $this->get('/api/tags/' . $tag->getId());
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains(["name" => "Tag1"]);
    }

    public function testCreate(): void
    {
        $this->init();

        $this->post("/api/tags", ["name" => "Tag1"]);

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains(["name" => "Tag1"]);

        $this->get('/api/tags');
        $this->assertJsonContains(["hydra:member" => [
            ["name" => "Tag1"]
        ]]);
    }

    public function testDelete(): void
    {
        $this->init();

        $tag = TagFactory::createOne(["name" => "Tag1"]);

        $this->delete("/api/tags/" . $tag->getId());
        $this->assertResponseIsSuccessful();

        $this->get('/api/tags');
        $this->assertJsonContains(["hydra:totalItems" => 0]);
    }

    public function testUpdate(): void
    {
        $this->init();

        $tag = TagFactory::createOne(["name" => "Tag1"]);

        $this->put("/api/tags/" . $tag->getId(), ["name" => "Tag2"]);

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains(["name" => "Tag2"]);

        $this->get('/api/tags');
        $this->assertJsonContains(["hydra:member" => [
            ["name" => "Tag2"]
        ]]);
    }
}
